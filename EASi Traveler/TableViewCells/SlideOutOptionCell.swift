//
//  SlideOutOptionCell.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/17/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class SlideOutOptionCell: UITableViewCell {
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var optionIcon: UIImageView!
    @IBOutlet weak var optionName: UILabel!
}
