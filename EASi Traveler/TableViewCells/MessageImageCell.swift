//
//  MessageImageCell.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 3/10/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class MessageImageCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
}
