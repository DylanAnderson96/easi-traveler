//
//  MessagesCell.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 3/6/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class MessagesCell: UITableViewCell {
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var sender: UIImageView! {
        didSet {
            self.sender.layer.cornerRadius = 20
            self.sender.layer.borderColor = UIColor.black.cgColor
            self.sender.layer.borderWidth = 0.5
            self.sender.clipsToBounds = true
        }
    }
    @IBOutlet weak var notificationTitle: UILabel!
    @IBOutlet weak var notificationBody: UILabel!
    
}
