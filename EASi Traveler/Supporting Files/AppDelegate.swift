//
//  AppDelegate.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/14/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //            self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        
        return true
    }
    
    
    //MARK: - Push Notifications
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    private func application(application: UIApplication,
                             didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    //TODO: - Notifications with more content
    //https://medium.com/@tsif/ios-10-rich-push-notifications-with-media-attachments-a54dc86586c2
    @available(iOS 10, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let application = UIApplication.shared
        let userInfo = response.notification.request.content.userInfo
                    //This is what the dictionary looks like if you want to parse it and display the image or use the customKey-CustomValue to look for certain events to be triggered
        //            {
        //                aps =     {
        //                    alert =         {
        //                        body = "Notification with an image";
        //                        title = "Notification With Image";
        //                    };
        //                    badge = 1;
        //                    "mutable-content" = 1;
        //                    sound = default;
        //                };
        //                customKey = CustomValue;
        //                "fcm_options" =     {
        //                    image = "https://firebasestorage.googleapis.com/v0/b/easi-traveler-95505.appspot.com/o/abd.jpg?alt=media&token=942871fd-46b1-4837-a721-46e41db11c0c";
        //                };
        //                "gcm.message_id" = 1583359518218337;
        //                "gcm.n.e" = 1;
        //                "gcm.notification.sound2" = default;
        //                "google.c.a.c_id" = 8070345502880622023;
        //                "google.c.a.c_l" = "Notification with an image";
        //                "google.c.a.e" = 1;
        //                "google.c.a.ts" = 1583359517;
        //                "google.c.a.udt" = 0;
        //                "google.c.sender.id" = 394657401655;
        //            }
        if (application.applicationState == .active) || (application.applicationState == .background) || (application.applicationState == .inactive) {
            // user tapped the notification bar when the app is in foreground
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SomeNotification"), object: nil, userInfo: userInfo)
            UserDefaults.standard.set(Constants.UserDefaults.emergencyTravelSlideoutMenuOptionValue, forKey: Constants.UserDefaults.emergencyTravelSlideoutMenuOption)
        }
        
        //might have foreground be red notification badge
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SomeNotification"), object: nil, userInfo: userInfo)
        UserDefaults.standard.set(Constants.UserDefaults.emergencyTravelSlideoutMenuOptionValue, forKey: Constants.UserDefaults.emergencyTravelSlideoutMenuOption)
    }
            
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}




