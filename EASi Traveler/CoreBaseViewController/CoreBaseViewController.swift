//
//  CoreBaseViewController.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/17/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import NVActivityIndicatorView

class CoreBaseViewController: UIViewController, UINavigationControllerDelegate, UINavigationBarDelegate, NVActivityIndicatorViewable {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(listenForRemoteNotifications(notification:)), name: NSNotification.Name("SomeNotification"), object: nil)
    }
    
    @objc func listenForRemoteNotifications(notification: NSNotification) {
        print("Your user notification is \(notification.userInfo)")
        if let dict = notification.userInfo as NSDictionary? {
            print("dictionary \(dict)")
            //This is what the dictionary looks like if you want to parse it and display the image or use the customKey-CustomValue to look for certain events to be triggered
//            {
//                aps =     {
//                    alert =         {
//                        body = "Notification with an image";
//                        title = "Notification With Image";
//                    };
//                    badge = 1;
//                    "mutable-content" = 1;
//                    sound = default;
//                };
//                customKey = CustomValue;
//                "fcm_options" =     {
//                    image = "https://firebasestorage.googleapis.com/v0/b/easi-traveler-95505.appspot.com/o/abd.jpg?alt=media&token=942871fd-46b1-4837-a721-46e41db11c0c";
//                };
//                "gcm.message_id" = 1583359518218337;
//                "gcm.n.e" = 1;
//                "gcm.notification.sound2" = default;
//                "google.c.a.c_id" = 8070345502880622023;
//                "google.c.a.c_l" = "Notification with an image";
//                "google.c.a.e" = 1;
//                "google.c.a.ts" = 1583359517;
//                "google.c.a.udt" = 0;
//                "google.c.sender.id" = 394657401655;
//            }
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: Constants.Segues.MessageViewControllerVC) as! MessageViewController
//        let menuImage: UIImage = UIImage(systemName: "list.dash")!.withRenderingMode(.alwaysOriginal)
//        mainViewController.addLeftBarButtonWithImage(menuImage)
        let navController = UINavigationController(rootViewController: mainViewController)
        let leftViewController = storyboard.instantiateViewController(withIdentifier: Constants.Segues.SlideOutMenuVC) as! SlideOutMenuVC
        let slideMenuController = SlideMenuController(mainViewController: navController, leftMenuViewController: leftViewController)
        
        UserDefaults.standard.set(Constants.UserDefaults.emergencyTravelSlideoutMenuOptionValue, forKey: Constants.UserDefaults.emergencyTravelSlideoutMenuOption)
        
        slideMenuController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        slideMenuController.modalPresentationStyle = .fullScreen
        self.present(slideMenuController, animated: true, completion: nil)
        
    }
    
    func showLoadingIndicator(){
            startAnimating(CGSize(width: 50, height: 50), message: nil, type: NVActivityIndicatorType.ballScaleRippleMultiple, color: UIColor.white , fadeInAnimation: nil)
    }
    func removeLoadingIndicator(){
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

    }
    
}
