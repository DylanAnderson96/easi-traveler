//
//  ElevatorPitchVC.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/18/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import WebKit

class EmailCalendarContactsVC: CoreBaseViewController, WKUIDelegate, WKNavigationDelegate, UITabBarDelegate {
    
    @IBOutlet weak var inboxBtn: UIBarButtonItem!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.openWebView(url: "https://outlook.office365.com/mail/inbox")
    }
    
    func openWebView(url: String) {
        self.showLoadingIndicator()
        if let url = URL(string: url) {
            let request = URLRequest(url: url)
            webView.allowsBackForwardNavigationGestures = true
            webView.allowsLinkPreview = true
            webView.contentMode = .scaleAspectFit
            webView.navigationDelegate = self
            webView.uiDelegate = self
            webView.load(request)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.removeLoadingIndicator()
    }
    
    @IBAction func menuPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func inboxPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let messagesVC = storyboard.instantiateViewController(identifier: Constants.Segues.MessageViewControllerVC) as! MessageViewController
        messagesVC.modalPresentationStyle = .fullScreen
        messagesVC.isSelectedFromMenu = false
        self.navigationController?.pushViewController(messagesVC, animated: true)
    }
}
