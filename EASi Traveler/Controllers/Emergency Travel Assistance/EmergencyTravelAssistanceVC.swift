//
//  ElevatorPitchVC.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/18/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import MessageUI

class EmergencyTravelAssistanceVC: CoreBaseViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var inboxBtn: UIBarButtonItem!
    @IBOutlet weak var menBtn: UIBarButtonItem!
    @IBOutlet weak var allegisCorporateNum: UIButton!
    @IBOutlet weak var rentalCarNum: UIButton!
    @IBOutlet weak var emailCorprate: UIButton!
    @IBOutlet weak var afterHoursNum: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addvisuals()
    }
    
    func addvisuals() {
        self.view.backgroundColor = .darkBlue

        let email = "Email: corptravel@allegisgroup.com"
        let selectWord1 = "corptravel@allegisgroup.com"
        self.emailCorprate.titleLabel?.colorString(text: email, coloredText: selectWord1, color: .link, isUnderlined: true)
        
        let corpNum = "Phone: 410-579-3100"
        let selectWord2 = "410-579-3100"
        self.allegisCorporateNum.titleLabel?.colorString(text: corpNum, coloredText: selectWord2, color: .link, isUnderlined: true)
        
        let corpNumRental = "Phone: 410-579-3100"
        let selectWord3 = "410-579-3100"
        self.rentalCarNum.titleLabel?.colorString(text: corpNumRental, coloredText: selectWord3, color: .link, isUnderlined: true)
        
        let afterHours = "*Phone: 203-786-6333"
        let selectWord4 = "203-786-6333"
        self.afterHoursNum.titleLabel?.colorString(text: afterHours, coloredText: selectWord4, color: .link, isUnderlined: true)
    }
    
    @IBAction func phoneAllegisCorporate(_ sender: Any) {
        self.callNumber(number: "4105793100")
    }
    
    @IBAction func phoneEmergencyHours(_ sender: Any) {
        self.displayAlertWithClosure(title: "Are you sure?", message: "Please use this number only if it is after 6pm or the weekend.", alertType: .actionSheet, options: ["Yes", "Cancel"], alertStyles: [.default, .destructive]) { (option) in
            switch option {
            case 0:
                self.callNumber(number: "2037866333")
            case 1:
                print("Do nothing")
            default:
                print("Do Nothing")
            }
        }
    }
    
    func callNumber(number: String) {
        if let url = URL(string: "tel://\(number)") {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @IBAction func emailCorporate(_ sender: Any) {
        self.displayAlertWithClosure(title: "Send an email or copy the email to clipboard", message: "Please select the easiest option", alertType: .actionSheet, options: ["Send Email", "Clipboard", "Cancel"], alertStyles: [.default, .default, .destructive]) { (option) in
            if option == 0 {
                self.openMailAppWithDetails(subject: "", body: "")
            } else if option == 1 {
                let stringToCopyToClipboard: String = "corptravel@allegisgroup.com"
                UIPasteboard.general.string = stringToCopyToClipboard
                self.displayAlert(title: "Success", message: "Successfully saved the email to your clipboard.")
            }
        }
    }
    
    func openMailAppWithDetails(subject: String, body: String) {
        let mailComposeViewController = configureMailComposer(emailSubject: subject, emailBody: body)
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        }else{
            self.displayAlert(title: "Whoops", message: "There was an error trying to send the email. Please make sure you have the 'Mail' app downloaded to send an email.")
        }
    }
    
    func configureMailComposer(emailSubject: String, emailBody: String) -> MFMailComposeViewController {
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients(["corptravel@allegisgroup.com"])
        mailComposeVC.setSubject(emailSubject)
        mailComposeVC.setMessageBody(emailBody, isHTML: false)
        return mailComposeVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func menuPressed(_ sender: Any) {
        self.openLeft()
    }
    @IBAction func inboxPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let messagesVC = storyboard.instantiateViewController(identifier: Constants.Segues.MessageViewControllerVC) as! MessageViewController
        messagesVC.modalPresentationStyle = .fullScreen
        messagesVC.isSelectedFromMenu = false
        self.navigationController?.pushViewController(messagesVC, animated: true)
    }
    
}
