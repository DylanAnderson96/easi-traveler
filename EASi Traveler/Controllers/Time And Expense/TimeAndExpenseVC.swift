//
//  ElevatorPitchVC.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/18/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import WebKit

class TimeAndExpenseVC: CoreBaseViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet var webView: WKWebView!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var inboxBtn: UIBarButtonItem!
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        self.openWebView()
    }
    
    override func loadView() {
        webView = WKWebView()
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.uiDelegate = self
        view = webView
    }
    
    
    func openWebView() {
        self.showLoadingIndicator()
        let urlString = "https://timeandexpense.aerotek.com/webtime/"
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            webView.allowsBackForwardNavigationGestures = true
            webView.allowsLinkPreview = true
            webView.contentMode = .scaleAspectFit
            webView.navigationDelegate = self
            webView.load(request)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.removeLoadingIndicator()
    }
    
    @IBAction func menuPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func inboxPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let messagesVC = storyboard.instantiateViewController(identifier: Constants.Segues.MessageViewControllerVC) as! MessageViewController
        messagesVC.modalPresentationStyle = .fullScreen
        messagesVC.isSelectedFromMenu = false
        self.navigationController?.pushViewController(messagesVC, animated: true)
    }
}
