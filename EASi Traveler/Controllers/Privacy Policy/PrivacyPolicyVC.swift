//
//  ElevatorPitchVC.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/18/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyVC: CoreBaseViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var inboxBtn: UIBarButtonItem!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var webView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.openWebView()
    }
    
    func openWebView() {
        self.showLoadingIndicator()
        let urlString = "https://easi-traveler.flycricket.io/privacy.html"
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            webView.allowsBackForwardNavigationGestures = true
            webView.allowsLinkPreview = true
            webView.contentMode = .scaleAspectFit
            webView.navigationDelegate = self
            webView.uiDelegate = self
            webView.load(request)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.removeLoadingIndicator()
    }
    
    @IBAction func menuPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func inboxPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let messagesVC = storyboard.instantiateViewController(identifier: Constants.Segues.MessageViewControllerVC) as! MessageViewController
        messagesVC.modalPresentationStyle = .fullScreen
        messagesVC.isSelectedFromMenu = false
        self.navigationController?.pushViewController(messagesVC, animated: true)
    }
}
