//
//  CustomizeAnnotation.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/18/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import CoreLocation
 
class CustomizeAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var phone: String!
    var name: String!
    var address: String!
    var image: UIImage!
    var tag: Int!
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
