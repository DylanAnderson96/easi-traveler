//
//  EASI.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/18/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import Foundation

class EASI: NSObject {
    
    struct Keys {
        static var address = "address"
        static var position = "position"
    }
    
    var address: String!
    var position: Dictionary<String, Any>!
    
    init(_ json: [String: Any]) {
        self.address = json[Keys.address] as? String ?? ""
        self.position = json[Keys.position] as? Dictionary<String, Any> ?? [:]
    }
    
    convenience init(json: [String: Any]) {
        self.init(json)
    }
}
