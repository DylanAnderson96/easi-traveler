//
//  ElevatorPitchVC.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/18/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class WheresEASiVC: CoreBaseViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var inboxBtn: UIBarButtonItem!
    @IBOutlet weak var easiAeroTek: UISegmentedControl!
    let locationManger = CLLocationManager()
    var easiLocations = [EASI]()
    var aeroTekLocations = [EASI]()
    @IBOutlet weak var locationMeBtn: UIButton!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
            mapView.mapType = .standard
            mapView.isPitchEnabled = false
            mapView.isRotateEnabled = false
            mapView.isScrollEnabled = true
            mapView.isZoomEnabled = true
            mapView.showsBuildings = true
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpMapView()
        self.easiAeroTek.selectedSegmentIndex = 0
        self.addPins(isEASI: true, isAeroTek: false)
        let shouldSlideOutbeOpen = UserDefaults.standard.bool(forKey: "keepSlideOutMenuOpen")
        if shouldSlideOutbeOpen {
            self.openLeft()
        }
    }
    
    func setUpMapView() {
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let selectedTitleAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        self.easiAeroTek.setTitleTextAttributes(titleTextAttributes, for: .normal)
        self.easiAeroTek.setTitleTextAttributes(selectedTitleAttributes, for: .selected)
        self.easiAeroTek.layer.borderColor = UIColor.black.cgColor
        self.easiAeroTek.layer.borderWidth = 1
        self.easiAeroTek.applyShadow()
        self.view.bringSubviewToFront(easiAeroTek)

        
        mapView.delegate = self
        mapView.isUserInteractionEnabled = true
        //Location Manager set up
        self.locationManger.delegate = self
        self.locationManger.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManger.requestWhenInUseAuthorization()
        self.locationManger.requestAlwaysAuthorization()
        self.locationManger.startUpdatingLocation()
        self.mapView.showsUserLocation = true
        self.mapView.showsTraffic = true
        self.mapView.showsScale = true
        
        //UI for lcoate me button
        self.locationMeBtn.tintColor = .link
        let locationImage = UIImage(systemName: "location")
        self.locationMeBtn.setImage(locationImage, for: .normal)
        self.locationMeBtn.layer.cornerRadius = 5
        self.locationMeBtn.backgroundColor = .systemBackground
        self.locationMeBtn.applyShadow()
    }
    
    @IBAction func inboxPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let messagesVC = storyboard.instantiateViewController(identifier: Constants.Segues.MessageViewControllerVC) as! MessageViewController
        messagesVC.modalPresentationStyle = .fullScreen
        messagesVC.isSelectedFromMenu = false
        self.navigationController?.pushViewController(messagesVC, animated: true)
    }
    
    func getAllEASILocations() {
        self.easiLocations.removeAll()
        
        if let easiPath = Bundle.main.path(forResource: "easi-locations", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: easiPath), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let locations = jsonResult as? [Dictionary<String, Any>] {
                    for location in locations {
                        easiLocations.append(EASI(json: location))
                    }
                } else {
                    print("no json")
                }
            } catch {
                print("There was an error getting the data")
            }
        } else {
            print("error gettting json file")
        }
    }
    
    func getAllAEROLocations() {
        self.aeroTekLocations.removeAll()
        
        if let aeroPath = Bundle.main.path(forResource: "aerotek-locations", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: aeroPath), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let locations = jsonResult as? [Dictionary<String, Any>] {
                    for location in locations {
                        aeroTekLocations.append(EASI(json: location))
                    }
                } else {
                    print("no json")
                }
            } catch {
                print("There was an error getting the data")
            }
        } else {
            print("error gettting json file")
        }
    }
    
    @IBAction func easiOrAeroTek(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.addPins(isEASI: true, isAeroTek: false)
        } else {
            self.addPins(isEASI: false, isAeroTek: true)
        }
    }
    
    func addPins(isEASI: Bool, isAeroTek: Bool) {
        self.mapView.removeAnnotations(mapView.annotations)
        let locationImage = UIImage(systemName: "location")
        self.locationMeBtn.setImage(locationImage, for: .normal)
        
        if isEASI {
            self.getAllEASILocations()
            
            for (index, easiLocation) in easiLocations.enumerated() {
                let lat: Double = easiLocation.position?["lat"] as? Double ?? 0.0
                let long: Double = easiLocation.position?["lng"] as? Double ?? 0.0
                let pinAnnotation = CustomizeAnnotation(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
                pinAnnotation.tag = index
                pinAnnotation.address = easiLocation.address
                pinAnnotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                mapView.addAnnotation(pinAnnotation)
            }
            
            //These co-ords are the center of the USA and used to center the map at this point with edge padding all the way around from the closest pin to each side
            let center = CLLocationCoordinate2D(latitude: 32.7339, longitude: -97.1211)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 80, longitudeDelta: 80))
            self.mapView.showAnnotations(mapView.annotations, animated: true)
            self.mapView.setRegion(region, animated: true)
        } else {
            self.getAllAEROLocations()
            
            for (index, aeroTekLocation) in aeroTekLocations.enumerated() {
                
                let lat: Double = aeroTekLocation.position?["lat"] as? Double ?? 0.0
                let long: Double = aeroTekLocation.position?["lng"] as? Double ?? 0.0
                let pinAnnotation = CustomizeAnnotation(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
                pinAnnotation.address = aeroTekLocation.address
                pinAnnotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                pinAnnotation.tag = index
                mapView.addAnnotation(pinAnnotation)
            }
            
            //These co-ords are the center of the USA and used to center the map at this point with edge padding all the way around from the closest pin to each side
            let center = CLLocationCoordinate2D(latitude: 40.773904, longitude: -112.016602)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 120, longitudeDelta: 120))
            self.mapView.showAnnotations(mapView.annotations, animated: true)
            self.mapView.setRegion(region, animated: true)
        }
    }
    
    @IBAction func locateMePressed(_ sender: Any) {
        let locationImage = UIImage(systemName: "location.fill")
        self.locationMeBtn.setImage(locationImage, for: .normal)
        let region = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
        self.mapView.setRegion(region, animated: true)
    }
    
    @IBAction func menuPressed(_ sender: Any) {
        self.openLeft()
    }
    
    func addPaddingToMapView() {
        self.mapView.layoutMargins = UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 25)
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //        let location = locations.last!
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isEqual(mapView.userLocation) {
            return nil
        } else {
            let annotationView = AnnotationView(annotation: annotation, reuseIdentifier: "Pin")
            annotationView.isEnabled = true
            annotationView.canShowCallout = false
            annotationView.annotation = annotation
            let pinImage = UIImage(named: "redPin")
            let size = CGSize(width: 30, height: 30)
            UIGraphicsBeginImageContext(size)
            pinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            annotationView.image = resizedImage
            annotationView.contentMode = .scaleAspectFit
            
            return annotationView
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation?.isEqual(mapView.userLocation) ?? false {
            //Do nothing if user selects their own location
            return
        } else {
            let customAnnotation = view.annotation as! CustomizeAnnotation
            let calloutView = UIView(frame: CGRect(x: 16, y: view.frame.minY - 40, width: self.view.frame.width - 32, height: 50))
            calloutView.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
            calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.5)
            calloutView.layer.cornerRadius = 8
            calloutView.applyShadow()
            calloutView.tag = customAnnotation.tag
            view.addSubview(calloutView)
            mapView.setCenter((view.annotation?.coordinate)!, animated: true)
            
            let addressLbl = UILabel(frame: CGRect(x: 60, y: 0, width: calloutView.frame.width - 70, height: 50))
            addressLbl.text = customAnnotation.address
            addressLbl.font = UIFont.systemFont(ofSize: 17)
            addressLbl.adjustsFontSizeToFitWidth = true
            addressLbl.numberOfLines = 2
            addressLbl.textColor = .black
            addressLbl.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
            calloutView.addSubview(addressLbl)
            
            
            let blueCarDistanceView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            blueCarDistanceView.backgroundColor = .link
            blueCarDistanceView.layer.cornerRadius = 8
            blueCarDistanceView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
            calloutView.addSubview(blueCarDistanceView)
            
            let carImg = UIImage(systemName: "car")?.withRenderingMode(.alwaysTemplate)
            let carImageView = UIImageView(frame: CGRect(x: 10, y: 0, width: 30, height: 30))
            carImageView.image = carImg
            carImageView.contentMode = .scaleAspectFit
            carImageView.tintColor = .white
            blueCarDistanceView.addSubview(carImageView)
            
            guard let currentLocation = mapView.userLocation.location else { return }
            let location = self.coordsToCLLocation(lat: customAnnotation.coordinate.latitude, long: customAnnotation.coordinate.longitude)
            
            let distance = self.drvingDistanceBetweenPoints(location1: currentLocation, location2: location)
            let distanceLbl = UILabel(frame: CGRect(x: 5, y: 31, width: 40, height: 19))
            distanceLbl.text = "\(distance) miles"
            distanceLbl.font = UIFont.systemFont(ofSize: 12)
            distanceLbl.adjustsFontSizeToFitWidth = true
            distanceLbl.numberOfLines = 2
            distanceLbl.textAlignment = .center
            distanceLbl.textColor = .white
            blueCarDistanceView.addSubview(distanceLbl)
            
            let callOutTapGesture = UITapGestureRecognizer(target: self, action: #selector(callOutViewTapped(sender:)))
            calloutView.isUserInteractionEnabled = true
            calloutView.addGestureRecognizer(callOutTapGesture)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.isKind(of: AnnotationView.self) {
            for subview in view.subviews {
                subview.removeFromSuperview()
            }
        }
    }
    
    func drvingDistanceBetweenPoints(location1: CLLocation, location2: CLLocation) -> Int {
        let distance = location2.distance(from: location1)
        return Int(round(distance * 0.000798371))
    }
    
    func coordsToCLLocation(lat: Double, long: Double) -> CLLocation {
        let location = CLLocation(latitude: lat, longitude: long)
        return location
    }
    
    @objc func callOutViewTapped(sender: UITapGestureRecognizer) {
        
        if self.easiAeroTek.selectedSegmentIndex == 0 {
            let address: String = self.easiLocations[sender.view?.tag ?? 0].address ?? ""
            let lat: Double = self.easiLocations[sender.view?.tag ?? 0].position["lat"] as! Double
            let long: Double = self.easiLocations[sender.view?.tag ?? 0].position["lng"] as! Double
            
            self.determineWhichMapsToOpen(title: "Pick Your Favorite Map Type", message: "Select your preferred map app to start navigation to a selected pin", alertType: .actionSheet, address: address, lat: lat, long: long)
        } else {
            let address: String = self.aeroTekLocations[sender.view?.tag ?? 0].address ?? ""
            let lat: Double = self.aeroTekLocations[sender.view?.tag ?? 0].position["lat"] as! Double
            let long: Double = self.aeroTekLocations[sender.view?.tag ?? 0].position["lng"] as! Double
            
            self.determineWhichMapsToOpen(title: "Pick Your Favorite Map Type", message: "Select your preferred map app to start navigation to a selected pin", alertType: .actionSheet, address: address, lat: lat, long: long)
        }
    }
    
    func determineWhichMapsToOpen(title: String, message: String, alertType: UIAlertController.Style, address: String, lat: Double, long: Double){
        let alert = UIAlertController(title: title, message: message, preferredStyle: alertType)
        
        if alertType == .alert {
            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { (_) in
                
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Apple Maps", style: .default, handler: { (_) in
                self.openAppleMaps(address: address, lat: lat, long: long)
            }))
            //            alert.addAction(UIAlertAction(title: "Waze Maps", style: .default, handler: { (_) in
            //                self.openWaze(address: address)
            //            }))
            alert.addAction(UIAlertAction(title: "Google Maps", style: .default, handler: { (_) in
                self.opengoogleMaps(address: address)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in
                
            }))
        }
        self.present(alert, animated: true)
    }
    
    func openWaze(address: String) {
        let CLLocation = convertAddressToCLLocation(address: address)
        let location = CLLocation.coordinate
        if UIApplication.shared.canOpenURL(URL(string: "waze://")!) {
            // Waze is installed. Launch Waze and start navigation
            let urlStr: String = "waze://?ll=\(location.latitude),\(location.longitude)&navigate=yes"
            UIApplication.shared.open(URL(string: urlStr)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            self.determineWhichMapsToOpen(title: "Whoops", message: "Looks like you need to install the Waze App to continue", alertType: .alert, address: "", lat: 0.0, long: 0.0)
        }
    }
    
    func openAppleMaps(address:String, lat: Double, long: Double) {
        
        let regionDistance: CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(lat, long)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = address
        mapItem.openInMaps(launchOptions: options)
    }
    
    func opengoogleMaps(address:String){
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?q=\(address.replacingOccurrences(of: " ", with: "+"))")!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.open(URL(string:"comgooglemaps://?q=\(address.replacingOccurrences(of: " ", with: "+"))")!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
        
    }
    
    fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
    }
    
    func convertAddressToCLLocation(address: String) -> CLLocation {
        var Cllocation = CLLocation()
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            if let placemarks = placemarks, let location: CLLocation = placemarks.first?.location {
                Cllocation = location
            }
        }
        return Cllocation
    }
    
}
