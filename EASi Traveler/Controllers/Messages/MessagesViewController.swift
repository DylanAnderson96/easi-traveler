//
//  MessagesViewController.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 3/6/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import CoreGraphics
import FirebaseFirestoreSwift
import Firebase
import Kingfisher
import UserNotifications

class MessageViewController: CoreBaseViewController, UISearchBarDelegate {
    
    @IBOutlet weak var searchBtn: UIBarButtonItem!
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchBarContraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopContraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var searchBarShowing: Bool = false
    var isSelectedFromMenu: Bool = true
    var messages = [Messages]()
    var sortedMessages = [Messages]()
    var currentMessages: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        self.firebaseNotificationListener()
        self.askForNotificationPermission()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func firebaseNotificationListener() {
        if !self.isKeyPresentInUserDefaults(key: Constants.UserDefaults.numberOfNotifications) {
            UserDefaults.standard.set(0, forKey: Constants.UserDefaults.numberOfNotifications)
        }

        let db = Firestore.firestore()
        db.collection(Constants.FirestoreCollections.notification).addSnapshotListener { (snapshot, error) in
            if error != nil {
                print("There was an error getting the data from Firebase, \(String(describing: error?.localizedDescription))")
                return
            } else {
                self.currentMessages = UserDefaults.standard.integer(forKey: Constants.UserDefaults.numberOfNotifications)

                self.messages.removeAll()

                guard let documents = snapshot?.documents else { return }
                for document in documents {
                    self.messages.append(Messages(document.data()))
                }
                
                if self.currentMessages < documents.count {
                    let title = documents.last?.data()["title"] as? String ?? "New Changes"
                    let body = documents.last?.data()["body"] as? String ?? "We have more interesting information in the messages tab. Please check them out when you get time."
                    let launchImage = documents.last?.data()["sender"] as? String ?? "ralph"

                    self.sendLocalNotification(title: title, body: body, launchImage: launchImage)
                    UserDefaults.standard.set(self.messages.count, forKey: Constants.UserDefaults.numberOfNotifications)
                }
            
                self.tableView.reloadData()
            }
        }
    }
    
    func sendLocalNotification(title: String, body: String, launchImage: String) {
        #warning("add red circle with badge for foreground use notifications")
        #warning("use pod for empty table view")
        //To schedule notifications for the future use this:
        //https://medium.com/quick-code/local-notifications-with-swift-4-b32e7ad93c2
        let notificationCenter = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.badge = 1
        content.sound = UNNotificationSound.default
        content.launchImageName = launchImage
        let request = UNNotificationRequest(identifier: "local notification", content: content, trigger: nil)
        notificationCenter.add(request) { (error) in
            if error != nil {
                print("There was am error showing local notifications")
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(listenForRemoteNotifications(notification:)), name: NSNotification.Name("SomeNotification"), object: nil)
        
    }
    
    func askForNotificationPermission() {
        let notificationCenter = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .announcement, .carPlay, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) { (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
    }
    
    func addNotificationToFirebase(row: Int) {
        let timeRightNow = Date().toString(dateFormat: "yyyy-MM-dd HH:mm")
        
        let value: [String: Any] = ["title": "Title \(row)", "body": "body \(row)", "dateAndTime": timeRightNow, "links": ["www.google.com", "www.youtube.com"], "images": ["imageNameFromStorage1", "imageNameFromStorage2"], "sender": "ralph"]
        
        let db = Firestore.firestore()
        db.collection(Constants.FirestoreCollections.notification).addDocument(data: value)
    }
    
    func setUpView() {
        self.searchBar.delegate = self
        self.searchBar.showsCancelButton = true
        self.searchBar.backgroundImage = UIImage()
        
        self.navigationController?.navigationBar.applyShadow(width: 0, height: 2, shadowRadius: 2, shadowColor: .label)

        if self.isSelectedFromMenu {
            self.dismissBtn.image = UIImage(systemName: "list.dash")?.withRenderingMode(.alwaysOriginal)
        } else {
            self.dismissBtn.image = UIImage(systemName: "arrow.left")?.withRenderingMode(.alwaysOriginal)
        }
        
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 0.5))
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 0.5))
        self.tableView.tableHeaderView?.backgroundColor = .label
        self.tableView.tableFooterView?.backgroundColor = .label

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UIView.animate(withDuration: 0.5) {
            if self.searchBarShowing {
                self.searchBar.transform = CGAffineTransform(translationX: 0, y: 0)
                self.tableView.transform = CGAffineTransform(translationX: 0, y: 0)
                self.searchBarShowing = false
            } else {
                self.searchBar.transform = CGAffineTransform(translationX: 0, y: 48)
                self.tableView.transform = CGAffineTransform(translationX: 0, y: 44)
                self.searchBarShowing = true
            }
        }
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        if self.isSelectedFromMenu {
            self.openLeft()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            if self.searchBarShowing {
                self.searchBar.transform = CGAffineTransform(translationX: 0, y: 0)
                self.tableView.transform = CGAffineTransform(translationX: 0, y: 0)
                self.searchBarShowing = false
            } else {
                self.searchBar.transform = CGAffineTransform(translationX: 0, y: 48)
                self.tableView.transform = CGAffineTransform(translationX: 0, y: 44)
                self.searchBarShowing = true
            }
        }
    }
    
    func converyTimeIntervalToDate(date: String) -> String {
        let today = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        let myDate = dateFormatter.date(from: date)!
        
        let components1 = Calendar.current.component(.day, from: today)
        let components2 = Calendar.current.component(.day, from: myDate)
        
        if components1 == components2 {
            dateFormatter.dateFormat = "h:mm a"
        } else {
            dateFormatter.dateFormat = "MM/dd/yy"
        }

        let newDate = dateFormatter.string(from: myDate)
        return newDate
    }
    
}

extension MessageViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "messagesCell", for: indexPath) as! MessagesCell
        sortedMessages = messages.sorted(by: { $0.dateAndTime > $1.dateAndTime })
        
        cell.notificationTitle.text = sortedMessages[indexPath.row].title
        cell.notificationBody.text = sortedMessages[indexPath.row].body
        cell.timeLbl.text = self.converyTimeIntervalToDate(date: sortedMessages[indexPath.row].dateAndTime)

        let ref = Storage.storage().reference().child(sortedMessages[indexPath.row].sender+".jpeg")
        ref.downloadURL { (url, error) in
            if let error = error {
                print("There was an error getting the storage ref image \(String(describing: error.localizedDescription))")
                return
            } else {
                let absoluteURL = URL(string: url!.absoluteString)!
                cell.sender.kf.setImage(with: absoluteURL)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let displayMessageVC = storyboard.instantiateViewController(identifier: Constants.Segues.DisplayMessagesVC) as! DisplayMessagesVC
        displayMessageVC.messageTitle = self.sortedMessages[indexPath.row].title
        displayMessageVC.messageBody = self.sortedMessages[indexPath.row].body
        displayMessageVC.senderPic = self.sortedMessages[indexPath.row].sender
        displayMessageVC.attachedImages = self.sortedMessages[indexPath.row].images ?? []
        displayMessageVC.attachedLinks = self.sortedMessages[indexPath.row].links ?? []
        self.navigationController?.pushViewController(displayMessageVC, animated: true)
    }
    
    
}
