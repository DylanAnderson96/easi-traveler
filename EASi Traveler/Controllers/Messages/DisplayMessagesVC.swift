//
//  DisplayMessagesVC.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 3/10/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase

enum LinkNumber: Int {
    case noLinks = 0
    case link1 = 1
    case link2 = 2
    case link3 = 3
}

class DisplayMessagesVC: CoreBaseViewController {
    
    #warning("When a new notification comes in, take user here and not to messages")
    @IBOutlet weak var senderImage: UIImageView!
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var notificationBody: UITextView!
    @IBOutlet weak var slideShowCollectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var linksLbl: UILabel!
    @IBOutlet weak var link1: UIButton!
    @IBOutlet weak var link2: UIButton!
    @IBOutlet weak var link3: UIButton!
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBOutlet weak var linksBtnTopConstraint: NSLayoutConstraint!
    
    var messageTitle: String = ""
    var messageBody: String = ""
    var senderPic: String = ""
    var attachedLinks: [String] = []
    var attachedImages: [String] = []
    var timer = Timer()
    var counter = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.slideShowCollectionView.register(MessageImageCell.self, forCellWithReuseIdentifier: "imageCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpView()
    }
    
    func setUpView() {
        self.senderImage.applyShadow()
        self.navigationController?.navigationBar.applyShadow(width: 0, height: 2, shadowRadius: 2, shadowColor: .label)
        self.linksLbl.text = "Attached Links"
        
        self.navTitle.text = messageTitle
        self.notificationBody.translatesAutoresizingMaskIntoConstraints = false
        self.notificationBody.isScrollEnabled = false
        self.notificationBody.text = messageBody
        self.notificationBody.sizeToFit()
        self.notificationBody.layoutIfNeeded()
        let height = notificationBody.sizeThatFits(CGSize(width: notificationBody.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height
        notificationBody.contentSize.height = height
        
        let ref = Storage.storage().reference().child(senderPic+".jpeg")
        ref.downloadURL { (url, error) in
            if let error = error {
                print("There was an error getting the storage ref image \(String(describing: error.localizedDescription))")
                return
            } else {
                print(url!.absoluteString)
                let absoluteURL = URL(string: url!.absoluteString)!
                self.senderImage.kf.setImage(with: absoluteURL)
            }
        }
        
        
        self.timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        
        self.pageControl.currentPageIndicatorTintColor = .lightBlue
        self.pageControl.pageIndicatorTintColor = .white
        self.slideShowCollectionView.delegate = self
        self.slideShowCollectionView.dataSource = self

        if self.attachedImages.count == 0 {
            self.pageControl.isHidden = true
            self.slideShowCollectionView.isHidden = true
            self.linksBtnTopConstraint.constant = 10
        } else {
            self.pageControl.numberOfPages = self.attachedImages.count
            self.linksBtnTopConstraint.constant = 270
            self.slideShowCollectionView.reloadData()
        }
         
        self.link1.setTitleColor(.link, for: .normal)
        self.link2.setTitleColor(.link, for: .normal)
        self.link3.setTitleColor(.link, for: .normal)
        self.link1.tag = 1
        self.link2.tag = 2
        self.link3.tag = 3
        if self.attachedLinks.count == LinkNumber.noLinks.rawValue {
            self.linksLbl.isHidden = true
            self.link1.isHidden = true
            self.link2.isHidden = true
            self.link3.isHidden = true
        } else if self.attachedLinks.count == LinkNumber.link1.rawValue {
            self.link1.setTitle(attachedLinks[0], for: .normal)
            self.link1.isHidden = false
            self.link2.isHidden = true
            self.link3.isHidden = true
        } else if self.attachedLinks.count == LinkNumber.link2.rawValue {
            self.link1.setTitle(attachedLinks[0], for: .normal)
            self.link2.setTitle(attachedLinks[1], for: .normal)
            self.link1.isHidden = false
            self.link2.isHidden = false
            self.link3.isHidden = true
        } else {
            self.link1.setTitle(attachedLinks[0], for: .normal)
            self.link2.setTitle(attachedLinks[1], for: .normal)
            self.link3.setTitle(attachedLinks[2], for: .normal)
            self.link1.isHidden = false
            self.link2.isHidden = false
            self.link3.isHidden = false
        }
    }
    
    @objc func changeImage() {
        #warning("fix this scroll view and also make this tappable to take up the whole screen to show image")
        if counter < attachedImages.count {
            counter += 1
            self.pageControl.currentPage = counter
            let index = IndexPath.init(item: counter, section: 0)
            self.slideShowCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
        } else {
            counter = 0
            self.pageControl.currentPage = counter
            let index = IndexPath.init(item: counter, section: 0)
            self.slideShowCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
        }
    }
    
    @IBAction func linkAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let displayMessageVC = storyboard.instantiateViewController(identifier: Constants.Segues.MessageLinkView) as! MessageLinkView
        self.navigationController?.pushViewController(displayMessageVC, animated: true)
        if sender.tag == LinkNumber.link1.rawValue {
            displayMessageVC.webviewString = self.attachedLinks[0]
        } else if sender.tag == LinkNumber.link2.rawValue {
            displayMessageVC.webviewString = self.attachedLinks[1]
        } else {
            displayMessageVC.webviewString = self.attachedLinks[2]
        }
    }
    
    @IBAction func dismissBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension DisplayMessagesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.attachedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! MessageImageCell
        
        let ref = Storage.storage().reference().child(attachedImages[indexPath.row]+".jpeg")
        ref.downloadURL { (url, error) in
            if let error = error {
                print("There was an error getting the storage ref image \(String(describing: error.localizedDescription))")
                return
            } else {
                let absoluteURL = URL(string: url!.absoluteString)!
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: cell.contentView.frame.width, height: cell.contentView.frame.height))
                imageView.contentMode = .scaleToFill
                imageView.kf.setImage(with: absoluteURL)
                imageView.applyShadow(width: 0, height: 3, shadowRadius: 3, shadowColor: .label)
                cell.contentView.addSubview(imageView)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 20, height: 230)
    }
    
}
