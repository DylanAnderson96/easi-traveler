//
//  MessageLinkView.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 3/10/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import WebKit

class MessageLinkView: CoreBaseViewController, WKNavigationDelegate, WKUIDelegate {
    
    @IBOutlet weak var dismissBtn: UIBarButtonItem!
    @IBOutlet weak var webView: WKWebView!
    var webviewString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.openWebView()
    }
    
    func openWebView() {
        self.showLoadingIndicator()
        let urlString = webviewString
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            webView.allowsBackForwardNavigationGestures = true
            webView.allowsLinkPreview = true
            webView.contentMode = .scaleAspectFit
            webView.navigationDelegate = self
            webView.uiDelegate = self
            webView.load(request)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.removeLoadingIndicator()
    }
    
    @IBAction func dismissBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
