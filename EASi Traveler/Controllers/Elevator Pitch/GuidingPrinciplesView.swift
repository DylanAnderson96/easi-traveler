//
//  GuidingPrinciplesView.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/20/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

extension ElevatorPitchVC {
    
    func addScrollViewForGuidingPrinciples() {
        let seperatorColor: UIColor = .white
        let headerColor: UIColor = .orangeEASiOrange
        let textColor: UIColor = .white
        let headerSize: CGFloat = 24
        let textSize: CGFloat = 16
        let bulletPoint = "• "
        let numberOfBulletPoints = (22*25)
        let headerToFirstBulletPoint = 9*(40+5+5+2+5)
        
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        scrollView.delegate = self
        scrollView.contentSize = CGSize(width: Int(self.view.frame.width), height: headerToFirstBulletPoint + numberOfBulletPoints + 20)
        scrollView.backgroundColor = .darkBlue
        scrollView.alwaysBounceHorizontal = false
        scrollView.alwaysBounceVertical = true
        self.guidingPriciplesView.addSubview(scrollView)
        
        //MARK: - Honor Your Promises
        let honorLabel = UILabel(frame: CGRect(x: 5, y: 10, width: view.frame.width - 30, height: 30))
        honorLabel.textAlignment = .left
        honorLabel.textColor = headerColor
        honorLabel.text = "Honor your promises"
        honorLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        honorLabel.adjustsFontSizeToFitWidth = true
        honorLabel.numberOfLines = 2
        honorLabel.backgroundColor = .clear
        scrollView.addSubview(honorLabel)

        let honorView = UIView(frame: CGRect(x: 5, y: honorLabel.frame.maxY + 5, width: self.view.frame.width - 30, height: 2))
        honorView.backgroundColor = seperatorColor
        scrollView.addSubview(honorView)
        
        let honorDescrip1 = UILabel(frame: CGRect(x: 13, y: honorView.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        honorDescrip1.textAlignment = .left
        honorDescrip1.textColor = textColor
        honorDescrip1.text = bulletPoint + "Deliver great results... the right way"
        honorDescrip1.font = UIFont.systemFont(ofSize: textSize)
        honorDescrip1.adjustsFontSizeToFitWidth = true
        honorDescrip1.numberOfLines = 2
        scrollView.addSubview(honorDescrip1)
        
        let honorDescrip2 = UILabel(frame: CGRect(x: 13, y: honorDescrip1.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        honorDescrip2.textAlignment = .left
        honorDescrip2.textColor = textColor
        honorDescrip2.text = bulletPoint + "You're only as good as your word"
        honorDescrip2.font = UIFont.systemFont(ofSize: textSize)
        honorDescrip2.adjustsFontSizeToFitWidth = true
        honorDescrip2.numberOfLines = 2
        scrollView.addSubview(honorDescrip2)
        
        let honorDescrip3 = UILabel(frame: CGRect(x: 13, y: honorDescrip2.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        honorDescrip3.textAlignment = .left
        honorDescrip3.textColor = textColor
        honorDescrip3.text = bulletPoint + "Commit to quality"
        honorDescrip3.font = UIFont.systemFont(ofSize: textSize)
        honorDescrip3.adjustsFontSizeToFitWidth = true
        honorDescrip3.numberOfLines = 2
        scrollView.addSubview(honorDescrip3)
        
        let honorDescrip4 = UILabel(frame: CGRect(x: 13, y: honorDescrip3.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        honorDescrip4.textAlignment = .left
        honorDescrip4.textColor = textColor
        honorDescrip4.text = bulletPoint + "Outwork the competetion"
        honorDescrip4.font = UIFont.systemFont(ofSize: textSize)
        honorDescrip4.adjustsFontSizeToFitWidth = true
        honorDescrip4.numberOfLines = 2
        scrollView.addSubview(honorDescrip4)
        
        //MARK: - Develop yourself and others
        let developLabel = UILabel(frame: CGRect(x: 5, y: honorDescrip4.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        developLabel.textAlignment = .left
        developLabel.textColor = headerColor
        developLabel.text = "Develop yourself and others"
        developLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        developLabel.adjustsFontSizeToFitWidth = true
        developLabel.numberOfLines = 2
        developLabel.backgroundColor = .clear
        scrollView.addSubview(developLabel)

        let developView = UIView(frame: CGRect(x: 5, y: developLabel.frame.maxY + 5, width: view.frame.width - 30, height: 2))
        developView.backgroundColor = seperatorColor
        scrollView.addSubview(developView)
        
        let developDescrip1 = UILabel(frame: CGRect(x: 13, y: developView.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        developDescrip1.textAlignment = .left
        developDescrip1.textColor = textColor
        developDescrip1.text = bulletPoint + "Know your craft"
        developDescrip1.font = UIFont.systemFont(ofSize: textSize)
        developDescrip1.adjustsFontSizeToFitWidth = true
        developDescrip1.numberOfLines = 2
        scrollView.addSubview(developDescrip1)
        
        let developDescrip2 = UILabel(frame: CGRect(x: 13, y: developDescrip1.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        developDescrip2.textAlignment = .left
        developDescrip2.textColor = textColor
        developDescrip2.text = bulletPoint + "Share knowledge with others"
        developDescrip2.font = UIFont.systemFont(ofSize: textSize)
        developDescrip2.adjustsFontSizeToFitWidth = true
        developDescrip2.numberOfLines = 2
        scrollView.addSubview(developDescrip2)
        
        //MARK: - Build a legacy
        let legacyLabel = UILabel(frame: CGRect(x: 5, y: developDescrip2.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        legacyLabel.textAlignment = .left
        legacyLabel.textColor = headerColor
        legacyLabel.text = "Build a legacy"
        legacyLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        legacyLabel.adjustsFontSizeToFitWidth = true
        legacyLabel.numberOfLines = 2
        legacyLabel.backgroundColor = .clear
        scrollView.addSubview(legacyLabel)

        let legacyView = UIView(frame: CGRect(x: 5, y: legacyLabel.frame.maxY + 5, width: view.frame.width - 30, height: 2))
        legacyView.backgroundColor = seperatorColor
        scrollView.addSubview(legacyView)
        
        let legacyDescrip1 = UILabel(frame: CGRect(x: 13, y: legacyView.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        legacyDescrip1.textAlignment = .left
        legacyDescrip1.textColor = textColor
        legacyDescrip1.text = bulletPoint + "Build great relationships"
        legacyDescrip1.font = UIFont.systemFont(ofSize: textSize)
        legacyDescrip1.adjustsFontSizeToFitWidth = true
        legacyDescrip1.numberOfLines = 2
        scrollView.addSubview(legacyDescrip1)
        
        let legacyDescrip2 = UILabel(frame: CGRect(x: 13, y: legacyDescrip1.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        legacyDescrip2.textAlignment = .left
        legacyDescrip2.textColor = textColor
        legacyDescrip2.text = bulletPoint + "Make a positive impact"
        legacyDescrip2.font = UIFont.systemFont(ofSize: textSize)
        legacyDescrip2.adjustsFontSizeToFitWidth = true
        legacyDescrip2.numberOfLines = 2
        scrollView.addSubview(legacyDescrip2)
        
        let legacyDescrip3 = UILabel(frame: CGRect(x: 13, y: legacyDescrip2.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        legacyDescrip3.textAlignment = .left
        legacyDescrip3.textColor = textColor
        legacyDescrip3.text = bulletPoint + "Help promote those around you"
        legacyDescrip3.font = UIFont.systemFont(ofSize: textSize)
        legacyDescrip3.adjustsFontSizeToFitWidth = true
        legacyDescrip3.numberOfLines = 2
        scrollView.addSubview(legacyDescrip3)
        
        //MARK: - Give and receive feedback
        let feedackLabel = UILabel(frame: CGRect(x: 5, y: legacyDescrip3.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        feedackLabel.textAlignment = .left
        feedackLabel.textColor = headerColor
        feedackLabel.text = "Give and receive feedback"
        feedackLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        feedackLabel.adjustsFontSizeToFitWidth = true
        feedackLabel.numberOfLines = 2
        feedackLabel.backgroundColor = .clear
        scrollView.addSubview(feedackLabel)

        let feedbackView = UIView(frame: CGRect(x: 5, y: feedackLabel.frame.maxY + 5, width: view.frame.width - 30, height: 2))
        feedbackView.backgroundColor = seperatorColor
        scrollView.addSubview(feedbackView)
        
        let feedbackDescrip1 = UILabel(frame: CGRect(x: 13, y: feedbackView.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        feedbackDescrip1.textAlignment = .left
        feedbackDescrip1.textColor = textColor
        feedbackDescrip1.text = bulletPoint + "Inform partners of their impact on others"
        feedbackDescrip1.font = UIFont.systemFont(ofSize: textSize)
        feedbackDescrip1.adjustsFontSizeToFitWidth = true
        feedbackDescrip1.numberOfLines = 2
        scrollView.addSubview(feedbackDescrip1)
        
        let feedbackDescrip2 = UILabel(frame: CGRect(x: 13, y: feedbackDescrip1.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        feedbackDescrip2.textAlignment = .left
        feedbackDescrip2.textColor = textColor
        feedbackDescrip2.text = bulletPoint + "Be approchable"
        feedbackDescrip2.font = UIFont.systemFont(ofSize: textSize)
        feedbackDescrip2.adjustsFontSizeToFitWidth = true
        feedbackDescrip2.numberOfLines = 2
        scrollView.addSubview(feedbackDescrip2)
        
        let feedbackDescrip3 = UILabel(frame: CGRect(x: 13, y: feedbackDescrip2.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        feedbackDescrip3.textAlignment = .left
        feedbackDescrip3.textColor = textColor
        feedbackDescrip3.text = bulletPoint + "Hold yourself and others accountable"
        feedbackDescrip3.font = UIFont.systemFont(ofSize: textSize)
        feedbackDescrip3.adjustsFontSizeToFitWidth = true
        feedbackDescrip3.numberOfLines = 2
        scrollView.addSubview(feedbackDescrip3)
        
        //MARK: - Treat everyone with respect
        let respectLabel = UILabel(frame: CGRect(x: 5, y: feedbackDescrip3.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        respectLabel.textAlignment = .left
        respectLabel.textColor = headerColor
        respectLabel.text = "Treat everyone with respect"
        respectLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        respectLabel.adjustsFontSizeToFitWidth = true
        respectLabel.numberOfLines = 2
        respectLabel.backgroundColor = .clear
        scrollView.addSubview(respectLabel)

        let respectView = UIView(frame: CGRect(x: 5, y: respectLabel.frame.maxY + 5, width: view.frame.width - 30, height: 2))
        respectView.backgroundColor = seperatorColor
        scrollView.addSubview(respectView)
        
        let repectDescrip1 = UILabel(frame: CGRect(x: 13, y: respectView.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        repectDescrip1.textAlignment = .left
        repectDescrip1.textColor = textColor
        repectDescrip1.text = bulletPoint + "Value differences"
        repectDescrip1.font = UIFont.systemFont(ofSize: textSize)
        repectDescrip1.adjustsFontSizeToFitWidth = true
        repectDescrip1.numberOfLines = 2
        scrollView.addSubview(repectDescrip1)
        
        let repectDescrip2 = UILabel(frame: CGRect(x: 13, y: repectDescrip1.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        repectDescrip2.textAlignment = .left
        repectDescrip2.textColor = textColor
        repectDescrip2.text = bulletPoint + "Encourage individuality"
        repectDescrip2.font = UIFont.systemFont(ofSize: textSize)
        repectDescrip2.adjustsFontSizeToFitWidth = true
        repectDescrip2.numberOfLines = 2
        scrollView.addSubview(repectDescrip2)
        
        //MARK: - Take ownership
        let ownershipLabel = UILabel(frame: CGRect(x: 5, y: repectDescrip2.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        ownershipLabel.textAlignment = .left
        ownershipLabel.textColor = headerColor
        ownershipLabel.text = "Take ownership"
        ownershipLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        ownershipLabel.adjustsFontSizeToFitWidth = true
        ownershipLabel.numberOfLines = 2
        ownershipLabel.backgroundColor = .clear
        scrollView.addSubview(ownershipLabel)

        let ownershipView = UIView(frame: CGRect(x: 5, y: ownershipLabel.frame.maxY + 5, width: view.frame.width - 30, height: 2))
        ownershipView.backgroundColor = seperatorColor
        scrollView.addSubview(ownershipView)
        
        let ownershipDescrip1 = UILabel(frame: CGRect(x: 13, y: ownershipView.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        ownershipDescrip1.textAlignment = .left
        ownershipDescrip1.textColor = textColor
        ownershipDescrip1.text = bulletPoint + "Be responsible for your actions"
        ownershipDescrip1.font = UIFont.systemFont(ofSize: textSize)
        ownershipDescrip1.adjustsFontSizeToFitWidth = true
        ownershipDescrip1.numberOfLines = 2
        scrollView.addSubview(ownershipDescrip1)
        
        let ownershipDescrip2 = UILabel(frame: CGRect(x: 13, y: ownershipDescrip1.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        ownershipDescrip2.textAlignment = .left
        ownershipDescrip2.textColor = textColor
        ownershipDescrip2.text = bulletPoint + "Never blame others"
        ownershipDescrip2.font = UIFont.systemFont(ofSize: textSize)
        ownershipDescrip2.adjustsFontSizeToFitWidth = true
        ownershipDescrip2.numberOfLines = 2
        scrollView.addSubview(ownershipDescrip2)

        //MARK: - Work together with pride and passion
        let prideLabel = UILabel(frame: CGRect(x: 5, y: ownershipDescrip2.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        prideLabel.textAlignment = .left
        prideLabel.textColor = headerColor
        prideLabel.text = "Work together with pride and passion"
        prideLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        prideLabel.adjustsFontSizeToFitWidth = true
        prideLabel.numberOfLines = 2
        prideLabel.backgroundColor = .clear
        scrollView.addSubview(prideLabel)

        let prideView = UIView(frame: CGRect(x: 5, y: prideLabel.frame.maxY + 5, width: view.frame.width - 30, height: 2))
        prideView.backgroundColor = seperatorColor
        scrollView.addSubview(prideView)
        
        let prideDescrip1 = UILabel(frame: CGRect(x: 13, y: prideView.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        prideDescrip1.textAlignment = .left
        prideDescrip1.textColor = textColor
        prideDescrip1.text = bulletPoint + "Take pride in what we do-our job job changes lives"
        prideDescrip1.font = UIFont.systemFont(ofSize: textSize)
        prideDescrip1.adjustsFontSizeToFitWidth = true
        prideDescrip1.numberOfLines = 2
        scrollView.addSubview(prideDescrip1)
        
        let prideDescrip2 = UILabel(frame: CGRect(x: 13, y: prideDescrip1.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        prideDescrip2.textAlignment = .left
        prideDescrip2.textColor = textColor
        prideDescrip2.text = bulletPoint + "Be a team player"
        prideDescrip2.font = UIFont.systemFont(ofSize: textSize)
        prideDescrip2.adjustsFontSizeToFitWidth = true
        prideDescrip2.numberOfLines = 2
        scrollView.addSubview(prideDescrip2)
        
        let prideDescrip3 = UILabel(frame: CGRect(x: 13, y: prideDescrip2.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        prideDescrip3.textAlignment = .left
        prideDescrip3.textColor = textColor
        prideDescrip3.text = bulletPoint + "Have a positive attitude"
        prideDescrip3.font = UIFont.systemFont(ofSize: textSize)
        prideDescrip3.adjustsFontSizeToFitWidth = true
        prideDescrip3.numberOfLines = 2
        scrollView.addSubview(prideDescrip3)
        
        let prideDescrip4 = UILabel(frame: CGRect(x: 13, y: prideDescrip3.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        prideDescrip4.textAlignment = .left
        prideDescrip4.textColor = textColor
        prideDescrip4.text = bulletPoint + "Have fun and celebrate successes"
        prideDescrip4.font = UIFont.systemFont(ofSize: textSize)
        prideDescrip4.adjustsFontSizeToFitWidth = true
        prideDescrip4.numberOfLines = 2
        scrollView.addSubview(prideDescrip4)
        
        //MARK: - Respect the past; create a better future
        let futureLabel = UILabel(frame: CGRect(x: 5, y: prideDescrip4.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        futureLabel.textAlignment = .left
        futureLabel.textColor = headerColor
        futureLabel.text = "Respect the past; create a better future"
        futureLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        futureLabel.adjustsFontSizeToFitWidth = true
        futureLabel.numberOfLines = 2
        futureLabel.backgroundColor = .clear
        scrollView.addSubview(futureLabel)

        let futureView = UIView(frame: CGRect(x: 5, y: futureLabel.frame.maxY + 5, width: view.frame.width - 30, height: 2))
        futureView.backgroundColor = seperatorColor
        scrollView.addSubview(futureView)
        
        let futureDescrip1 = UILabel(frame: CGRect(x: 13, y: futureView.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        futureDescrip1.textAlignment = .left
        futureDescrip1.textColor = textColor
        futureDescrip1.text = bulletPoint + "Protect and grow what has been built"
        futureDescrip1.font = UIFont.systemFont(ofSize: textSize)
        futureDescrip1.adjustsFontSizeToFitWidth = true
        futureDescrip1.numberOfLines = 2
        scrollView.addSubview(futureDescrip1)
        
        let futureDescrip2 = UILabel(frame: CGRect(x: 13, y: futureDescrip1.frame.maxY + 5, width: view.frame.width - 30, height: 20))
        futureDescrip2.textAlignment = .left
        futureDescrip2.textColor = textColor
        futureDescrip2.text = bulletPoint + "Make today better than yesterday"
        futureDescrip2.font = UIFont.systemFont(ofSize: textSize)
        futureDescrip2.adjustsFontSizeToFitWidth = true
        futureDescrip2.numberOfLines = 2
        scrollView.addSubview(futureDescrip2)
    }

    
}
