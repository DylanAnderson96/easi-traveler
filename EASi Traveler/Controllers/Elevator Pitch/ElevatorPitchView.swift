//
//  TimeAndExpenseView.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/25/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

extension ElevatorPitchVC {
    
    func addScrollViewForElevatorPitch() {
        let seperatorColor: UIColor = .white
        let headerColor: UIColor = .orangeEASiOrange
        let textColor: UIColor = .white
        let headerSize: CGFloat = 24
        let bulletPointTextSize: CGFloat = 20
        let emptyBulletTextSize: CGFloat = 16
        let bulletPoint = "• "
        let emptyBullet = "◦ "
        
        let scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 20))
        scrollView.delegate = self
        scrollView.contentSize = CGSize(width: Int(self.view.frame.width), height: 920)
        scrollView.backgroundColor = .darkBlue
        scrollView.alwaysBounceHorizontal = false
        scrollView.alwaysBounceVertical = true
        self.elevatorPitchView.addSubview(scrollView)
        
        //MARK: - Lines Of Business
        let linesOfBusinessLabel = UILabel(frame: CGRect(x: 5, y: 10, width: view.frame.width - 30, height: 30))
        linesOfBusinessLabel.textAlignment = .left
        linesOfBusinessLabel.textColor = headerColor
        linesOfBusinessLabel.text = "Lines Of Business"
        linesOfBusinessLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        linesOfBusinessLabel.adjustsFontSizeToFitWidth = true
        linesOfBusinessLabel.numberOfLines = 2
        linesOfBusinessLabel.backgroundColor = .clear
        scrollView.addSubview(linesOfBusinessLabel)
        
        let linesOfBusinessView = UIView(frame: CGRect(x: 5, y: linesOfBusinessLabel.frame.maxY + 5, width: self.view.frame.width - 30, height: 2))
        linesOfBusinessView.backgroundColor = seperatorColor
        scrollView.addSubview(linesOfBusinessView)
        
        let engineeringLine = UILabel(frame: CGRect(x: 10, y: linesOfBusinessView.frame.maxY + 5, width: view.frame.width - 30, height: 30))
        engineeringLine.textAlignment = .left
        engineeringLine.textColor = textColor
        engineeringLine.text = bulletPoint + "Engineering"
        engineeringLine.font = UIFont.boldSystemFont(ofSize: bulletPointTextSize)
        engineeringLine.adjustsFontSizeToFitWidth = true
        engineeringLine.numberOfLines = 2
        scrollView.addSubview(engineeringLine)

        let engineeringBullet1 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: engineeringLine.frame.maxY + 5, width: view.frame.width - 45, height: 25))
        engineeringBullet1.textAlignment = .left
        engineeringBullet1.textColor = textColor
        engineeringBullet1.text = emptyBullet + "Mechanical Design & Validation"
        engineeringBullet1.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        engineeringBullet1.adjustsFontSizeToFitWidth = true
        engineeringBullet1.numberOfLines = 2
        scrollView.addSubview(engineeringBullet1)
        
        let engineeringBullet2 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: engineeringBullet1.frame.maxY + 2, width: view.frame.width - 45, height: 25))
        engineeringBullet2.textAlignment = .left
        engineeringBullet2.textColor = textColor
        engineeringBullet2.text = emptyBullet + "Manufacturing Engineering"
        engineeringBullet2.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        engineeringBullet2.adjustsFontSizeToFitWidth = true
        engineeringBullet2.numberOfLines = 2
        scrollView.addSubview(engineeringBullet2)
        
        let engineeringBullet3 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: engineeringBullet2.frame.maxY + 2, width: view.frame.width - 45, height: 25))
        engineeringBullet3.textAlignment = .left
        engineeringBullet3.textColor = textColor
        engineeringBullet3.text = emptyBullet + "Embedded Systems"
        engineeringBullet3.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        engineeringBullet3.adjustsFontSizeToFitWidth = true
        engineeringBullet3.numberOfLines = 2
        scrollView.addSubview(engineeringBullet3)
        
        let engineeringBullet4 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: engineeringBullet3.frame.maxY + 2, width: view.frame.width - 45, height: 25))
        engineeringBullet4.textAlignment = .left
        engineeringBullet4.textColor = textColor
        engineeringBullet4.text = emptyBullet + "Electrical Engineering"
        engineeringBullet4.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        engineeringBullet4.adjustsFontSizeToFitWidth = true
        engineeringBullet4.numberOfLines = 2
        scrollView.addSubview(engineeringBullet4)
        
        let engineeringBullet5 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: engineeringBullet4.frame.maxY + 2, width: view.frame.width - 45, height: 25))
        engineeringBullet5.textAlignment = .left
        engineeringBullet5.textColor = textColor
        engineeringBullet5.text = emptyBullet + "Software Development"
        engineeringBullet5.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        engineeringBullet5.adjustsFontSizeToFitWidth = true
        engineeringBullet5.numberOfLines = 2
        scrollView.addSubview(engineeringBullet5)
        
        let sciencesLine = UILabel(frame: CGRect(x: 10, y: engineeringBullet5.frame.maxY + 5, width: view.frame.width - 30, height: 30))
        sciencesLine.textAlignment = .left
        sciencesLine.textColor = textColor
        sciencesLine.text = bulletPoint + "Sciences"
        sciencesLine.font = UIFont.boldSystemFont(ofSize: bulletPointTextSize)
        sciencesLine.adjustsFontSizeToFitWidth = true
        sciencesLine.numberOfLines = 2
        scrollView.addSubview(sciencesLine)

        let scienceBullet1 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: sciencesLine.frame.maxY + 5, width: view.frame.width - 45, height: 25))
        scienceBullet1.textAlignment = .left
        scienceBullet1.textColor = textColor
        scienceBullet1.text = emptyBullet + "Clinical"
        scienceBullet1.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        scienceBullet1.adjustsFontSizeToFitWidth = true
        scienceBullet1.numberOfLines = 2
        scrollView.addSubview(scienceBullet1)
        
        let scienceBullet2 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: scienceBullet1.frame.maxY + 2, width: view.frame.width - 45, height: 25))
        scienceBullet2.textAlignment = .left
        scienceBullet2.textColor = textColor
        scienceBullet2.text = emptyBullet + "Lab"
        scienceBullet2.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        scienceBullet2.adjustsFontSizeToFitWidth = true
        scienceBullet2.numberOfLines = 2
        scrollView.addSubview(scienceBullet2)
        
        let utilitiesLine = UILabel(frame: CGRect(x: linesOfBusinessView.frame.minX, y: scienceBullet2.frame.maxY + 5, width: view.frame.width - 30, height: 30))
        utilitiesLine.textAlignment = .left
        utilitiesLine.textColor = textColor
        utilitiesLine.text = bulletPoint + "Utilites"
        utilitiesLine.font = UIFont.boldSystemFont(ofSize: bulletPointTextSize)
        utilitiesLine.adjustsFontSizeToFitWidth = true
        utilitiesLine.numberOfLines = 2
        scrollView.addSubview(utilitiesLine)

        let utilitiesBullet1 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: utilitiesLine.frame.maxY + 5, width: view.frame.width - 45, height: 25))
        utilitiesBullet1.textAlignment = .left
        utilitiesBullet1.textColor = textColor
        utilitiesBullet1.text = emptyBullet + "Transmission"
        utilitiesBullet1.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        utilitiesBullet1.adjustsFontSizeToFitWidth = true
        utilitiesBullet1.numberOfLines = 2
        scrollView.addSubview(utilitiesBullet1)
        
        let utilitiesBullet2 = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: utilitiesBullet1.frame.maxY + 2, width: view.frame.width - 45, height: 25))
        utilitiesBullet2.textAlignment = .left
        utilitiesBullet2.textColor = textColor
        utilitiesBullet2.text = emptyBullet + "Distribution"
        utilitiesBullet2.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        utilitiesBullet2.adjustsFontSizeToFitWidth = true
        utilitiesBullet2.numberOfLines = 2
        scrollView.addSubview(utilitiesBullet2)
        
        
        //MARK: - Delivery Model
        let deliveryModelLabel = UILabel(frame: CGRect(x: 5, y: utilitiesBullet2.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        deliveryModelLabel.textAlignment = .left
        deliveryModelLabel.textColor = headerColor
        deliveryModelLabel.text = "Delivery Models"
        deliveryModelLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        deliveryModelLabel.adjustsFontSizeToFitWidth = true
        deliveryModelLabel.numberOfLines = 2
        deliveryModelLabel.backgroundColor = .clear
        scrollView.addSubview(deliveryModelLabel)
        
        let deliveryModelView = UIView(frame: CGRect(x: 5, y: deliveryModelLabel.frame.maxY + 5, width: self.view.frame.width - 30, height: 2))
        deliveryModelView.backgroundColor = seperatorColor
        scrollView.addSubview(deliveryModelView)
        
        let outsourcedModel = UILabel(frame: CGRect(x: 10, y: deliveryModelView.frame.maxY + 5, width: view.frame.width - 30, height: 30))
        outsourcedModel.textAlignment = .left
        outsourcedModel.textColor = textColor
        outsourcedModel.text = bulletPoint + "Outsourced"
        outsourcedModel.font = UIFont.boldSystemFont(ofSize: bulletPointTextSize)
        outsourcedModel.adjustsFontSizeToFitWidth = true
        outsourcedModel.numberOfLines = 2
        scrollView.addSubview(outsourcedModel)

        let outsourcedDescription = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: outsourcedModel.frame.maxY - 5, width: view.frame.width - 45, height: 40))
        outsourcedDescription.textAlignment = .left
        outsourcedDescription.textColor = textColor
        outsourcedDescription.text = emptyBullet + "Services provided outside of customer premise and viewed as coming with a higher level of accountability and risk for EASi."
        outsourcedDescription.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        outsourcedDescription.adjustsFontSizeToFitWidth = true
        outsourcedDescription.numberOfLines = 4
        scrollView.addSubview(outsourcedDescription)
        
        let outsourcedOutcomeModel = UILabel(frame: CGRect(x: 10, y: outsourcedDescription.frame.maxY + 5, width: view.frame.width - 30, height: 30))
        outsourcedOutcomeModel.textAlignment = .left
        outsourcedOutcomeModel.textColor = textColor
        outsourcedOutcomeModel.text = bulletPoint + "Onsite Managed Outcomes and Deliverable"
        outsourcedOutcomeModel.font = UIFont.boldSystemFont(ofSize: bulletPointTextSize)
        outsourcedOutcomeModel.adjustsFontSizeToFitWidth = true
        outsourcedOutcomeModel.numberOfLines = 2
        scrollView.addSubview(outsourcedOutcomeModel)

        let outsourcedOutcomeDescription = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: outsourcedOutcomeModel.frame.maxY - 5, width: view.frame.width - 45, height: 45))
        outsourcedOutcomeDescription.textAlignment = .left
        outsourcedOutcomeDescription.textColor = textColor
        outsourcedOutcomeDescription.text = emptyBullet + "Services provided solely on customer premises with defined deliverables and outcomes expected of a project."
        outsourcedOutcomeDescription.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        outsourcedOutcomeDescription.adjustsFontSizeToFitWidth = true
        outsourcedOutcomeDescription.numberOfLines = 4
        scrollView.addSubview(outsourcedOutcomeDescription)
        
        let outsourcedResourcesModel = UILabel(frame: CGRect(x: 10, y: outsourcedOutcomeDescription.frame.maxY - 5, width: view.frame.width - 30, height: 45))
        outsourcedResourcesModel.textAlignment = .left
        outsourcedResourcesModel.textColor = textColor
        outsourcedResourcesModel.text = bulletPoint + "Onsite Managed Resources"
        outsourcedResourcesModel.font = UIFont.boldSystemFont(ofSize: bulletPointTextSize)
        outsourcedResourcesModel.adjustsFontSizeToFitWidth = true
        outsourcedResourcesModel.numberOfLines = 2
        scrollView.addSubview(outsourcedResourcesModel)

        let outsourcedResourcesDescription = UILabel(frame: CGRect(x: engineeringLine.frame.minX + 20, y: outsourcedResourcesModel.frame.maxY - 5, width: view.frame.width - 45, height: 50))
        outsourcedResourcesDescription.textAlignment = .left
        outsourcedResourcesDescription.textColor = textColor
        outsourcedResourcesDescription.text = emptyBullet + "Onsite services provided without the defined expectations and outcomes of a project. Typically centered around resource managed activities."
        outsourcedResourcesDescription.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        outsourcedResourcesDescription.adjustsFontSizeToFitWidth = true
        outsourcedResourcesDescription.numberOfLines = 4
        scrollView.addSubview(outsourcedResourcesDescription)
        
        //MARK: - EASi Centers
        let easiCentersLabel = UILabel(frame: CGRect(x: 5, y: outsourcedResourcesDescription.frame.maxY + 10, width: view.frame.width - 30, height: 30))
        easiCentersLabel.textAlignment = .left
        easiCentersLabel.textColor = headerColor
        easiCentersLabel.text = "EASi Centers"
        easiCentersLabel.font = UIFont.boldSystemFont(ofSize: headerSize)
        easiCentersLabel.adjustsFontSizeToFitWidth = true
        easiCentersLabel.numberOfLines = 2
        easiCentersLabel.backgroundColor = .clear
        scrollView.addSubview(easiCentersLabel)
        
        let easiCentersView = UIView(frame: CGRect(x: 5, y: easiCentersLabel.frame.maxY + 5, width: self.view.frame.width - 30, height: 2))
        easiCentersView.backgroundColor = seperatorColor
        scrollView.addSubview(easiCentersView)
        
        let employeeCountLabel = UILabel(frame: CGRect(x: 10, y: easiCentersView.frame.maxY + 5, width: view.frame.width - 30, height: 30))
        employeeCountLabel.textAlignment = .left
        employeeCountLabel.textColor = textColor
        employeeCountLabel.text = bulletPoint + "3700+ Employees worldwide."
        employeeCountLabel.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        employeeCountLabel.adjustsFontSizeToFitWidth = true
        employeeCountLabel.numberOfLines = 2
        scrollView.addSubview(employeeCountLabel)

        let locationCountLabel = UILabel(frame: CGRect(x: 10, y: employeeCountLabel.frame.maxY + 5, width: view.frame.width - 30, height: 30))
        locationCountLabel.textAlignment = .left
        locationCountLabel.textColor = textColor
        locationCountLabel.text = bulletPoint + "22 Centers across USA, Canada and India."
        locationCountLabel.font = UIFont.systemFont(ofSize: emptyBulletTextSize)
        locationCountLabel.adjustsFontSizeToFitWidth = true
        locationCountLabel.numberOfLines = 2
        scrollView.addSubview(locationCountLabel)
        
    }
}
