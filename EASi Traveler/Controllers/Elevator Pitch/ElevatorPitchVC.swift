//
//  ElevatorPitchVC.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/18/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Firebase
import FirebaseStorage

class ElevatorPitchVC: CoreBaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var inboxBtn: UIBarButtonItem!
    @IBOutlet weak var elevatorPitchView: UIView!
    @IBOutlet weak var teamRulesView: UIView!
    @IBOutlet weak var guidingPriciplesView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBOutlet weak var teamRulesParagraph1: UILabel!
    @IBOutlet weak var teamRulesParagraph2: UILabel!
    @IBOutlet weak var teamRulesParagraph3: UILabel!
    @IBOutlet weak var playBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpVisuals()
        self.addScrollViewForElevatorPitch()
        self.addScrollViewForGuidingPrinciples()
    }
    
    func setUpVisuals() {
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let selectedTitleAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]

        self.segmentControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        self.segmentControl.setTitleTextAttributes(selectedTitleAttributes, for: .selected)
        self.segmentControl.layer.borderColor = UIColor.white.cgColor
        self.segmentControl.layer.borderWidth = 2

        self.playBtn.layer.cornerRadius = self.playBtn.frame.width/2
        self.playBtn.layer.borderWidth = 2
        self.playBtn.layer.borderColor = UIColor.orangeEASiOrange.cgColor
        self.playBtn.applyShadow(width: 0, height: 3, shadowRadius: 3, shadowColor: .white)
        self.playBtn.doGlowAnimation(withColor: .lightBlue)
        
        self.teamRulesView.backgroundColor = .darkBlue
        self.guidingPriciplesView.backgroundColor = .darkBlue
        self.elevatorPitchView.backgroundColor = .darkBlue
        self.view.backgroundColor = .darkBlue
        
        self.teamRulesParagraph1.textColor = .white
        self.teamRulesParagraph2.textColor = .white
        self.teamRulesParagraph3.textColor = .white

        let paragraph1 = "Every individual, regardless of position, is OBLIGATED to tell others how they impact him or her, positiviely or negatively."
        let paragraph2 = "These thoughts and feelings will be shared in a CARING spirit."
        let paragraph3 = "This information will be received as a sign of RESPECT."

        let coloredWordPar1 = "OBLIGATED"
        let coloredWordPar2 = "CARING"
        let coloredWordPar3 = "RESPECT"
        
        
        self.teamRulesParagraph1.colorString(text: paragraph1, coloredText: coloredWordPar1)
        self.teamRulesParagraph2.colorString(text: paragraph2, coloredText: coloredWordPar2)
        self.teamRulesParagraph3.colorString(text: paragraph3, coloredText: coloredWordPar3)
        
        self.teamRulesParagraph1.attributedText = addBoldText(fullString: NSString(string: teamRulesParagraph1!.text!), boldPartOfString: NSString(string: coloredWordPar1), font: .boldSystemFont(ofSize: 16), boldFont: .boldSystemFont(ofSize: 20))
        self.teamRulesParagraph2.attributedText = addBoldText(fullString: NSString(string: teamRulesParagraph2!.text!), boldPartOfString: NSString(string: coloredWordPar2), font: .boldSystemFont(ofSize: 16), boldFont: .boldSystemFont(ofSize: 20))
        self.teamRulesParagraph3.attributedText = addBoldText(fullString: NSString(string: teamRulesParagraph3!.text!), boldPartOfString: NSString(string: coloredWordPar3), font: .boldSystemFont(ofSize: 16), boldFont: .boldSystemFont(ofSize: 20))

    }
    
    @IBAction func segmentControlAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.elevatorPitchView.isHidden = false
            self.teamRulesView.isHidden = true
            self.guidingPriciplesView.isHidden = true
            self.navigationItem.title = "Elevator Pitch"
            self.playBtn.isHidden = false
        } else if sender.selectedSegmentIndex == 1 {
            self.elevatorPitchView.isHidden = true
            self.teamRulesView.isHidden = false
            self.guidingPriciplesView.isHidden = true
            self.navigationItem.title = "Team Rules"
            self.playBtn.isHidden = true
        } else {
            self.elevatorPitchView.isHidden = true
            self.teamRulesView.isHidden = true
            self.guidingPriciplesView.isHidden = false
            self.navigationItem.title = "Guiding Principles"
            self.playBtn.isHidden = true
        }
    }
    
    func addBoldText(fullString: NSString, boldPartOfString: NSString, font: UIFont!, boldFont: UIFont!, color: UIColor? = .lightBlue) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font:font!]
        let boldFontAttribute = [NSAttributedString.Key.font:boldFont!]
        let coloredWordAttibutes: [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor: color!]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartOfString as String))
        boldString.addAttributes(coloredWordAttibutes, range: fullString.range(of: boldPartOfString as String))
       return boldString
    }

    
    @IBAction func menuBtnPressed(_ sender: Any) {
        self.openLeft()
    }
    
    @IBAction func inboxPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let messagesVC = storyboard.instantiateViewController(identifier: Constants.Segues.MessageViewControllerVC) as! MessageViewController
        messagesVC.modalPresentationStyle = .fullScreen
        messagesVC.isSelectedFromMenu = false
        self.navigationController?.pushViewController(messagesVC, animated: true)
    }
    
    func playVideo() {
        let url = "https://firebasestorage.googleapis.com/v0/b/easi-traveler-95505.appspot.com/o/easiPitch.mp4?alt=media&token=9655ba5a-99c7-4f86-b1a4-11b02ff71e90"
        let player = AVPlayer(url: URL(string: url)!)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }

    
    @IBAction func playButtonPressed(_ sender: Any) {
        self.playVideo()
    }
}
