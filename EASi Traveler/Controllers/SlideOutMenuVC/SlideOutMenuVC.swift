//
//  SlideOutMenuVC.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/17/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class SlideOutMenuVC: CoreBaseViewController {
    
    @IBOutlet weak var easiIconImg: UIImageView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentVersion: UILabel!
    @IBOutlet weak var topView: UIView!
    var currentSelectedCell: Int?
    var selectedCellIndex: Int?
    
    var slideOutOptions: [String] = ["Concur",
                                     "Connected",
                                     "Time And Expenses",
                                     "Outlook",
                                     "Elevator Pitch",
                                     "Messages",
                                     "Where's EASi",
                                     "Emergency Travel Assistance",
                                     "Privacy Policy"
    ]
    
    var slideOutOptionControllerId: [String] = [Constants.Segues.ConcurViewController,
                                                Constants.Segues.ConnectedVC,
                                                Constants.Segues.TimeAndExpenseVC,
                                                Constants.Segues.EmailCalendarContactsVC,
                                                Constants.Segues.ElevatorPitchVC,
                                                Constants.Segues.MessageViewControllerVC,
                                                Constants.Segues.WheresEASiVC,
                                                Constants.Segues.EmergencyTravelAssistanceVC,
                                                Constants.Segues.PrivacyPolicyVC
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpVisuals()
        
        if self.isKeyPresentInUserDefaults(key: Constants.UserDefaults.emergencyTravelSlideoutMenuOption) {
            currentSelectedCell = Constants.UserDefaults.emergencyTravelSlideoutMenuOptionValue
            UserDefaults.standard.removeObject(forKey: Constants.UserDefaults.emergencyTravelSlideoutMenuOption)
        } else {
            currentSelectedCell = 6
        }
        
    }
    
    func setUpVisuals() {
        self.topView.applyGradientToView(colours: [.darkBlue, .lightBlue], fromX: 0, toX: 1, fromY: 0, toY: 1)
//        self.easiIconImg.applyShadow(width: 0, height: 3, shadowRadius: 3, shadowColor: .label)
        self.currentVersion.text = getCurrentVersionAndBuild()
        self.tableView.tableHeaderView = UIView()
        self.tableView.tableFooterView = UIView()
    }
    
    func getCurrentVersionAndBuild() -> String {
        let version: String = (Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String) ?? "1.0"
        let build: String = (Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String) ?? "(1)"
        return "Version " + version + " (\(build))"
    }
    
    func pushToViewController(storyboardID: String){
        closeLeft()
        UserDefaults.standard.set(false, forKey: "keepSlideOutMenuOpen")
        self.slideMenuController()?.changeMainViewController(UINavigationController(rootViewController: (self.storyboard?.instantiateViewController(withIdentifier: storyboardID))!), close: true)
    }
    
}

extension SlideOutMenuVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return slideOutOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "slideOutOptionCell", for: indexPath) as! SlideOutOptionCell
        
        cell.optionName.adjustsFontSizeToFitWidth = true
        cell.optionName.numberOfLines = 2
        cell.optionName.text = slideOutOptions[indexPath.row]
        if cell.optionName.text == "Time And Expenses" {
            cell.optionIcon.image = UIImage(named: "money")
        } else if cell.optionName.text == "Messages" {
            cell.optionIcon.image = UIImage(systemName: "envelope")
        } else {
            cell.optionIcon.image = UIImage(named: slideOutOptions[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        }
        cell.selectionStyle = .none
        if indexPath.row == currentSelectedCell {
            cell.backgroundColor = .orangeEASiOrange
            cell.bottomView.backgroundColor = .orangeEASiOrange
            cell.optionName.textColor = .white
            cell.optionIcon.tintColor = .black
        } else {
            cell.backgroundColor = .systemBackground
            cell.bottomView.backgroundColor = .systemBackground
            cell.optionName.textColor = .label
            cell.optionIcon.tintColor = .label
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToViewController(storyboardID: self.slideOutOptionControllerId[indexPath.row])
        self.currentSelectedCell = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "slideOutOptionCell", for: indexPath) as! SlideOutOptionCell
        if indexPath.row == currentSelectedCell {
            cell.backgroundColor = .orangeEASiOrange
            cell.bottomView.backgroundColor = .orangeEASiOrange
            cell.optionName.textColor = .white
        } else {
            cell.backgroundColor = .white
            cell.bottomView.backgroundColor = .white
            cell.optionName.textColor = .black
        }
        UserDefaults.standard.removeObject(forKey: Constants.UserDefaults.emergencyTravelSlideoutMenuOption)
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    
}
