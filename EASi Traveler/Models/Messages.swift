//
//  Messages.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 3/9/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class Messages: NSObject {
    
    struct Keys {
        static let title = "title"
        static let body = "body"
        static let images = "images"
        static let links = "links"
        static let sender = "sender"
        static let dateAndTime = "dateAndTime"
    }
    
    let title: String!
    let body: String!
    let images: [String]?
    let links: [String]?
    let sender: String!
    let dateAndTime: String!
    
    init(_ notificationDict: [String: Any]) {
        self.title = notificationDict[Keys.title] as? String
        self.body = notificationDict[Keys.body] as? String
        self.images = notificationDict[Keys.images] as? [String]
        self.links = notificationDict[Keys.links] as? [String]
        self.sender = notificationDict[Keys.sender] as? String
        self.dateAndTime = notificationDict[Keys.dateAndTime] as! String?
    }
    
    convenience init(vendorDictionary: [String: Any]) {
        self.init(vendorDictionary)
    }
}
