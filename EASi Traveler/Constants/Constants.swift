//
//  Constants.swift
//  EASi Traveler
//
//  Created by Dylan Anderson on 2/17/20.
//  Copyright © 2020 Dylan Anderson. All rights reserved.
//

import UIKit

class Constants {
    
    struct Segues {
        static let ConcurViewController = "ConcurViewController"
        static let SlideOutMenuVC = "SlideOutMenuVC"
        static let ConnectedVC = "ConnectedVC"
        static let TimeAndExpenseVC = "TimeAndExpenseVC"
        static let EmailCalendarContactsVC = "EmailCalendarContactsVC"
        static let WheresEASiVC = "WheresEASiVC"
        static let ElevatorPitchVC = "ElevatorPitchVC"
        static let EmergencyTravelAssistanceVC = "EmergencyTravelAssistanceVC"
        static let PrivacyPolicyVC = "PrivacyPolicyVC"
        static let MessageViewControllerVC = "MessageViewController"
        static let DisplayMessagesVC = "DisplayMessagesVC"
        static let MessageLinkView = "MessageLinkView"
    }
    
    struct UserDefaults {
        static let backgroundRemoteNotification = "backgroundNotification"
        static let emergencyTravelSlideoutMenuOptionValue = 5
        static let emergencyTravelSlideoutMenuOption = "emergencyTravelSlideoutMenuOption"
        static let numberOfNotifications = "numberOfNotifications"
    }
    
    struct FirestoreCollections {
        static let notification = "notification"
    }
    
}
